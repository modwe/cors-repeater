﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CorsRepeater.Models;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace CorsRepeater.Controllers
{
    public class HomeController : Controller
    {
        static HttpClient client = new HttpClient();

        public string Index()
        {
            return "404";
        }

        [HttpPost("/api/CORS")]
        public async Task<IActionResult> Cors([FromBody]Req req)
        {
            if(req==null || !ModelState.IsValid)
            {
                return NotFound();
            }

            HttpResponseMessage response = await client.GetAsync(req.path);
            string content = await response.Content.ReadAsStringAsync();

            dynamic data = JsonConvert.DeserializeObject(content);

            return Json(data);

        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
